# Consulta de produtos e serviços para compras públicas
Esse webservice foi idealizado de forma *independente, não vinculado à orgãos governamentais* para centralizar e unificar
o cadastro de produtos se tornando uma referência para a aquisição de bens e serviços na Administração Pública.

O objetivo é facilitar todo o processo de compras, simplificando os procedimentos de aquisição de bens e serviços.

Estarão disponíveis diversas categorias de produtos e serviços, com informação sobre fornecedores de bens e prestadores
de serviços e preços máximos estabelecidos em acordos.

### Atualmente estão disponíveis as seguintes categorias de bens e serviços: ###
*   Serviço Móvel Terrestre
*   Combustíveis Rodoviários
*   Higiene e Limpeza
*   Licenciamento de Software
*   Mobiliário
*   Vigilância e Segurança
*   Serviços de Comunicações de Voz e Dados
*   Refeições Confecionadas
*   Seguro Automóvel
*   Papel, Materiais de escritório e  de Impressão;
*   Cópia e Impressão
*   Equipamento Informático
*   Viagens e Alojamentos
*   Eletricidade
*   Veículos Automotores e Motocicletas

## Guia de referencia para consulta ##
TODO:

    http://catalogo.sicosp.com.br/categorias
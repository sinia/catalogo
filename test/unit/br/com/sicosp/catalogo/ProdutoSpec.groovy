package br.com.sicosp.catalogo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Produto)
class ProdutoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }

	void 'test inserção de unidade inválida falhas'() {
		when:
		Item item = [unidadeBase: 'kg'] as Item
		item.adicioneUnidade(unidadeInvalida)

		then:
		Exception excecao = thrown(IllegalArgumentException)
		excecao.message = "Unidade inválida informada '$unidadeInvalida'"

		where:
		unidadeInvalida = 'ui' //Unidade inexistente
	}
}

package br.com.sicosp.catalogo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Agrupador)
class AgrupadorSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void 'teste criação de agrupador tem o resultado esperado'() {
		given:
		Agrupador agrupador = new Agrupador(
				nome: 'Caminhão 3 eixos',
				unidade: 't',
				multiplicador: 4
		)
		expect:
		agrupador.validate()
		agrupador.toString() == '4 t'
	}

	void 'teste criação de agrupador com unidade customizada o resultado esperado'() {
		given:
		Agrupador agrupador = new Agrupador(
				nome: 'Caixa de ovos',
				unidade: 'dz',
				multiplicador: 1
		)
		expect:
		agrupador.validate()
		agrupador.toString() == '1 dz'
	}
}

import br.com.sicosp.catalogo.Categoria
import br.com.sicosp.catalogo.Produto

class BootStrap {

	def init = { servletContext ->
		Categoria categoria = new Categoria(nome: 'Alimentos').save()
		new Produto(
				nome: 'Arroz',
				categoria: categoria,
				especificacoes: [
						tipo: 'A'
				],
				unidadeBase: 'kg',
				unidades: ['t', 'g']
		).save()

		new Produto(
				nome: 'Feijão',
				categoria: categoria,
				unidadeBase: 'kg',
				unidades: ['t', 'g']
		).save()
	}
	def destroy = {
	}
}

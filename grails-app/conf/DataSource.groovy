// environment specific settings
environments {
    development {
		grails {
			mongo {
				host = "localhost"
				port = 27017
//				username = "blah"
//				password = "blah"
				databaseName = "test"
			}
		}
    }
    test {
		grails {
			mongo {
				host = "localhost"
				port = 27017
				username = "blah"
				password = "blah"
				databaseName = "test"
			}
		}
    }
    production {
		grails {
			mongo {
				//http://www.mongodb.org/display/DOCS/Master+Slave
				//http://docs.mongodb.org/manual/replication/
				//replicaPair = [ "localhost:27017", "localhost:27018"]
				host = "localhost"
				port = 27017
				username = "blah"
				password = "blah"
				databaseName = "production"
				options {
					autoConnectRetry = true
					connectTimeout = 300
				}
			}
		}
    }
}

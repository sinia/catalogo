class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/error')

//		"/produto"(resources:'produto')
//		"/categoria"(resources:'categoria')

		"/categoria"(resources:'categoria') {
			"/produto"(resources:"produto")
		}
	}
}

<!DOCTYPE html>
<html>
<title><g:if env="development">Grails Runtime Exception</g:if><g:else>Error</g:else></title>
<xmp theme="united" style="display:none;">
# 500
### Ocorreu um erro ao processar a requisição ###
<g:if env="development">
<g:renderException exception="${exception}"/>
</g:if>
</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
<body>
</body>
</html>

package br.com.sicosp.catalogo

import javax.measure.unit.NonSI

class Servico extends Item {

	Servico() {
		this.unidadeBase = NonSI.HOUR.toString()
	}

	static constraints = {
	}
}

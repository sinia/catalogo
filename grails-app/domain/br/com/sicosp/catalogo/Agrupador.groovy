package br.com.sicosp.catalogo

import org.bson.types.ObjectId
import org.jscience.physics.amount.Amount

import javax.measure.unit.Unit
import javax.measure.unit.UnitFormat

class Agrupador {
	ObjectId id
	String nome
	String descricao
	String unidade
	BigDecimal multiplicador

	static transients = ['amount', 'unit']

	static constraints = {
		nome blank: false
		descricao nullable: true
		unidade blank: false, validator: {
			UnitFormat.instance.isValidIdentifier(it)
		}
	}

	Amount getAmount() {
		Amount.valueOf(multiplicador.toFloat(), unit).setExact(multiplicador.toLong())
	}

	Unit getUnit() {
		Unit.valueOf(unidade)
	}

	String toString() {
		amount.toString()
	}
}

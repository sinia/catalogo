package br.com.sicosp.catalogo

import grails.rest.Resource

@Resource(uri='/produto')
class Produto extends Item {
	String marca
	static hasMany = [agrupadores: Agrupador]

	static constraints = {
		agrupadores cascade: 'none'
		marca nullable: true
	}
}

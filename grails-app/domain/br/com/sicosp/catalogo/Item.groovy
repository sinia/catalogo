package br.com.sicosp.catalogo

import org.bson.types.ObjectId

import javax.measure.unit.Unit
import javax.measure.unit.UnitFormat

abstract class Item {
	ObjectId id

	String nome
	String descricao
	Map especificacoes
	String unidadeBase
	List<String> unidades = []

	static belongsTo = [categoria: Categoria]
	static transients = ['baseUnit']

	static constraints = {
		nome blank: false
		descricao nullable: true
		especificacoes nullable: true
		unidadeBase blank: false, validator: {
			UnitFormat.instance.isValidIdentifier(it)
		}
	}

	Unit getBaseUnit() {
		Unit.valueOf(unidadeBase)
	}

	void adicioneUnidade(String unidade) {
		boolean unidadeValida = UnitFormat.instance.isValidIdentifier(unidade)
		if (!unidadeValida) {
			throw new IllegalArgumentException("Unidade inválida informada '$unidade'")
		}

		Unit unit = Unit.valueOf(unidade)

		if (unit.isCompatible(baseUnit)) {
			throw new IllegalArgumentException("Unidade '$unidade' imcompativel com a unidade base: '$unidadeBase")
		}
	}
}

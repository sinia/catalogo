package br.com.sicosp.catalogo

import grails.rest.Resource
import org.bson.types.ObjectId

@Resource(uri='/categoria')
class Categoria {
	ObjectId id
	String nome
	String descricao
	static hasMany = [itens: Item]

	static constraints = {
		nome blank: false
		descricao nullable: true
	}
}

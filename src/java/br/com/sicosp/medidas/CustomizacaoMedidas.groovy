package br.com.sicosp.medidas

import javax.measure.quantity.Dimensionless
import javax.measure.unit.Unit
import javax.measure.unit.UnitFormat
import static javax.measure.unit.NonSI.*

class CustomizacaoMedidas {
	public static Unit<Dimensionless> DUZIA = Unit.ONE.times(12)

	static {
		UnitFormat.instance.label(DUZIA, 'dz')

		UnitFormat.instance.alias(DUZIA, 'duzia')
		UnitFormat.instance.alias(YEAR, 'ano')
		UnitFormat.instance.alias(MONTH, 'mês')
		UnitFormat.instance.alias(WEEK, 'semana')
		UnitFormat.instance.alias(DAY, 'dia')
	}
}
